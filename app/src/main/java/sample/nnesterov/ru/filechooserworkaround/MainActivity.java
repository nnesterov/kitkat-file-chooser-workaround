package sample.nnesterov.ru.filechooserworkaround;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final int CHOOSE_FILE_REQUEST_CODE = 248;

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webView = (WebView) findViewById(R.id.webView);

        // Enable javascript to handle file upload
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("file:///android_asset/index.html");

        // Register java class as javascript interface
        webView.addJavascriptInterface(new JavaScriptFileChooserInterface(), "FileCallBack");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK
                && requestCode == CHOOSE_FILE_REQUEST_CODE
                && data != null) {
            Uri result = data.getData();
            // Here we can upload file manually and pass url of uploaded file to javascript
        }
    }

    private class JavaScriptFileChooserInterface {

        @SuppressWarnings("unused")
        @JavascriptInterface
        public void openFileChooser() {
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType("image/*");
            MainActivity.this.startActivityForResult( Intent.createChooser( i, "File Chooser" ), CHOOSE_FILE_REQUEST_CODE );
        }
    }
}
